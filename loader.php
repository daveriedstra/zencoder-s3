<?php
/**
 * Loads the plugin files
 *
 * @since 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Load utility functions
require_once( ZCS3_PLUGIN_DIR . 'includes/utils.php' );

// interface with linked media files
require_once( ZCS3_PLUGIN_DIR . 'includes/linked-files.php' );

// Load basic setup. Plugin list links, text domain, footer links etc.
require_once( ZCS3_PLUGIN_DIR . 'admin/basic-setup.php' );

// Load admin setup. Register menus and settings
require_once( ZCS3_PLUGIN_DIR . 'admin/admin-ui-setup.php' );

// Render Admin UI
require_once( ZCS3_PLUGIN_DIR . 'admin/admin-ui-render.php' );

// Set up admin meta boxes
require_once( ZCS3_PLUGIN_DIR . 'admin/meta-boxes.php' );

// Do plugin operations
require_once( ZCS3_PLUGIN_DIR . 'functions/do.php' );

// S3 utils
require_once( ZCS3_PLUGIN_DIR . 'includes/s3.php' );

// Cron task
// @TODO set up actual WP_Cron
require_once( ZCS3_PLUGIN_DIR . 'functions/cron-transfer.php' );
