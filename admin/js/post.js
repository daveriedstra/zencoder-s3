$(document).ready(() => {
  $('#zcs3-do-transcode-button').click(e => {
    e.preventDefault();

    var ID = ZCS3.postId;
    if (!ID) {
      alert("Couldn't create transcoding job -- script error: post ID not available");
      return
    }

    const ajaxFailed = (msg, err) => {
      $('#zcs3-do-transcode-button').removeAttr('disabled');
      $('#zcs3-do-transcode-button').parent().append(`<p><strong>${msg}</strong></p>`);
      console.error(err);
    }

    const handleZencoderResponse = data => {
      let msg = '';
      if (!!data.errors)
        msg = '<strong>Zencoder errors:</strong><br />' + data.errors.join('<br />')
      else
        msg = 'Jobs created! Refresh page to see status.';

      $('#zcs3-do-transcode-button').parent().append(`<p><em>${msg}</em></p>`);
    }

    // send ajax request to start encoding
    $('#zcs3-do-transcode-button').attr('disabled', 'disabled');
    $.post(
      ajaxurl,
      {
        action: 'zcs3_do_transcode',
        sec: ZCS3.sec,
        post_id: ZCS3.postId
      },
      res => {
        if (!res)
          return ajaxFailed('Unknown error occurred; please contact admin');

        res = JSON.parse(res)

        if (res.status !== 200)
          return ajaxFailed(res.message, res);

        if (typeof res.message === 'string')
          return ajaxFailed(res.message, res);

        handleZencoderResponse(res.message);
      }
    )
    .fail(err => ajaxFailed('Error in communicating with the server. Try again later or contact admin if the issue continues.', err))
  });
});
