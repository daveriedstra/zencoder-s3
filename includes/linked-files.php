<?php

/**
 * Initiates the linked files post meta for a post with a finished job
 */
function zcs3_set_linked_files($post, $job) {
    $linked_files = [];
    foreach($job['output_media_files'] as $output) {
        $linked_files[] = [
            'id' => $output['id'],
            'status' => ZCS3_STATUS_DOWNLOADING,
            'zencoder_state' => $output['state'],
            'details' => [
                'url' => $output['url'],
                'format' => $output['format'],
                'codec' => $output['video_codec'],
                'height' => $output['height'],
                'width' => $output['width'],
                'filesize' => $output['file_size_bytes'],
                'duration' => $output['duration_in_ms']
            ]
        ];
    }
    update_post_meta($post->ID, ZCS3_LINKED_FILES, json_encode($linked_files));
}

/**
 * Sets the url for the indicated linked file
 */
function zcs3_set_linked_file_url($post_id, $output_id, $furl) {
    $linked_files = json_decode(get_post_meta($post_id, ZCS3_LINKED_FILES, true), true);
    foreach($linked_files as &$lf) {
        if ($lf['id'] === $output_id) {
            $lf['url'] = $furl;
            break;
        }
    }
    update_post_meta($post_id, ZCS3_LINKED_FILES, json_encode($linked_files));
}

/**
 * Updates the status for the linked file associated with the specified output
 */
function zcs3_update_linked_files($post_id, $output_id, $status, $error_msg = '', $error_class = '') {
    $linked_files = json_decode(get_post_meta($post_id, ZCS3_LINKED_FILES, true), true);
    foreach($linked_files as &$lf) {
        if ($lf['id'] === $output_id) {
            $lf['status'] = $status;
            $lf['error_msg'] = $error_msg;
            $lf['error_class'] = $error_class;
            break;
        }
    }
    update_post_meta($post_id, ZCS3_LINKED_FILES, json_encode($linked_files));
}

function zcs3_get_linked_files($post_id) {
    return json_decode(get_post_meta($post_id, ZCS3_LINKED_FILES, true), true);
}

?>
