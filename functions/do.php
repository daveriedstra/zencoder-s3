<?php
/**
 * Operations of the plugin are included here.
 *
 * @since 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/**
 * Creates transcoding jobs with Zencoder for attachment $ID.
 *
 * Returns Zencoder response object or cURL error string.
 */
function zcs3_create_transcoding_jobs($ID) {
    // 1. get asset URL for $ID
    $attachment = get_post($ID);
    $source = $attachment->guid;

    // 2. get transcoding settings
    $settings = zcs3_get_settings();

    // 2a) video sizes
    $sizes = array_keys($settings['video_sizes'], "1");

    // 2b) video codecs
    $codecs = array_keys($settings['video_codecs'], "1");

    // c) Zencoder API key
    $api_key = $settings['zencoder_api_key'];

    // d) bucket
    $bucket = $settings['s3_bucket'];

    // e) let zencoder do S3 transfer? (only supports amazon)
    $use_zencoder_transfer = false; //|| $settings['use_zc_transfer'];

    // 3. for each codec, for each size, create a new job
    $outputs = [];
    foreach ($codecs as $codec) {
        foreach ($sizes as $size) {
            //  create new job
            $job_params = [
                'source' => $source,
                'height' => $size,
                'codec' => $codec,
                'key' => $api_key
            ];

            if ($use_zencoder_transfer) {
                $job_params['bucket'] = $bucket;
            }

            $outputs[] = zcs3_create_transcoding_output($job_params);
        }
    }

    $res = [];

    if (!defined('ZCS3_DRY_RUN')) {
        $res = zcs3_make_job_request($api_key, $source, $outputs);

        // if there are errors, just return the response
        if (is_string($res) || array_key_exists('errors', $res))
            return $res;
    } else {
        $res = ['id' => 'zcs3-dry-run-job-id'];
    }

    // if successful, update the video metadata with the ID and status
    update_post_meta($ID, ZCS3_ATTACHMENT_STATUS, ZCS3_STATUS_JOB_CREATED);
    update_post_meta($ID, ZCS3_ATTACHMENT_JOB_ID, $res['id']);

    return $res;
}

/**
 * Creates an output array for a zencoder job.
 *
 * params:
 * - codec
 * - height
 * - S3 bucket
 * - source (to make filename)
 */
function zcs3_create_transcoding_output($params) {
    $filename = zcs3_get_video_filename($params['source'], $params['height'], $params['codec']);
    $output = [
        'skip_audio' => true,
        'video_codec' => $params['codec'],
        'height' => $params['height'],
        'aspect_mode' => 'preserve',    // default
        'quality' => 3,                 // 1-5, default 3. 3 is "better than most web video"
        'filename' => $filename,
        'label' => $filename
    ];

    if (!empty($params['bucket']))
        $output['base_url'] = $params['bucket'];

    return $output;
}

/**
 * Creates one transcoding job with Zencoder.
 *
 * params:
 *  - source
 *  - bucket
 *  - height
 *  - codec
 *  - probably some others
 *
 *  Encoding jobs are created by sending an HTTP POST request to
 *  https://app.zencoder.com/api/v2/jobs. The post body must include one thing:
 *  the URL of a video to process. It may also include output settings for the
 *  job, including an output destination, notification settings, and transcoding
 *  settings. You also must send a Zencoder API Key in a Zencoder-Api-Key header.
 *
 *  https://docs.brightcove.com/zencoder-api/v2/doc/index.html#operation/createJob
 */
function zcs3_make_job_request($key, $source, $outputs) {
    $endpoint = ZCS3_ZENCODER_API_URL . 'jobs';

    $body = [
        'input' => $source,
        'test' => true,
        'output' => $outputs
    ];

    $header = [
        'Content-type: application/json',
        'Zencoder-Api-Key: ' . $key
    ];

    return zcs3_post($endpoint, $body, $header);
}

/**
 * returns a new filename for this source using the given height and codec.
 * if no codec is supplied, extension is not changed.
 */
function zcs3_get_video_filename($source, $height, $codec) {
    $codec_ext_map = [
        "h264" => ".mp4",
        "hevc" => ".mp4",
        "jp2" => ".mj2",
        "mpeg4" => ".mp4",
        "theora" => ".ogg",
        "vp6" => ".webm",
        "vp8" => ".webm",
        "vp9" => ".webm",
        "wmv" => ".mp4",
    ];

    if (array_key_exists($codec, $codec_ext_map))
        $ext = $codec_ext_map[$codec];
    else
        $ext = '.mp4';

    preg_match('/[^\/\\&\?]+\.\w{3,4}(?=([\?&].*$|$))/', $source, $matches);
    $base = '';

    if (isset($matches) && isset($matches[0]))
        $base = explode(".", $matches[0])[0];
    else
        $base = '' . time();

    return "$base-@{$height}p$ext";
}

/**
 * Gets job status for $job_id
 * If $job_id is empty, will return all jobs
 */
function zcs3_get_job_status($job_id, $api_key) {
    $endpoint = ZCS3_ZENCODER_API_URL . 'jobs';

    $header = [
        'Content-type: application/json',
        'Zencoder-Api-Key: ' . $api_key
    ];

    $res = zcs3_get("$endpoint/$job_id", "", $header);

    return json_decode($res, true);
}

/**
 * Encrypt sensitive data using openSSL
 */
function zcs3_encrypt($plaintext) {
    $cipher = 'aes-128-gcm';
    if (in_array($cipher, openssl_get_cipher_methods())) {
        $key = substr(hash('sha256', ZCS3_OPENSSL_KEY, true), 0, 32);
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $taglen = 16;
        $tag = "";
        $ciphertext = openssl_encrypt($plaintext, $cipher, $key, OPENSSL_RAW_DATA, $iv, $tag, "", $taglen);
        return base64_encode($iv . $tag . $ciphertext);
    } else {
        // fallback encryption
        wp_die(ZCS3_OPENSSL_CIPHER . ' not supported');
    }
}

function zcs3_decrypt($encrypted) {
    $cipher = 'aes-128-gcm';
    if (in_array($cipher, openssl_get_cipher_methods())) {
        $encrypted = base64_decode($encrypted);
        $key = substr(hash('sha256', ZCS3_OPENSSL_KEY, true), 0, 32);
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = substr($encrypted, 0, $ivlen);
        $taglen = 16;
        $tag = substr($encrypted, $ivlen, $taglen);
        $ciphertext = substr($encrypted, $ivlen + $taglen);
        return openssl_decrypt($ciphertext, $cipher, $key, OPENSSL_RAW_DATA, $iv, $tag);
    } else {
        // fallback to other encryption
        wp_die(ZCS3_OPENSSL_CIPHER . ' not supported');
    }
}
