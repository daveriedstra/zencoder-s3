<?php
/**
 * Admin setup for the plugin
 *
 * @since 1.0
 * @function	zcs3_add_menu_links()		Add admin menu pages
 * @function	zcs3_register_settings	Register Settings
 * @function	zcs3_validater_and_sanitizer()	Validate And Sanitize User Input Before Its Saved To Database
 * @function	zcs3_get_settings()		Get settings from database
 */

// Exit if accessed directly
if ( ! defined('ABSPATH') ) exit;

/**
 * Add admin menu pages
 *
 * @since 1.0
 * @refer https://developer.wordpress.org/plugins/administration-menus/
 */
function zcs3_add_menu_links() {
	add_options_page ( __('Zencoder S3','zencoder-s3'), __('Zencoder S3','zencoder-s3'), 'update_core', 'zencoder-s3','zcs3_admin_interface_render'  );
}
add_action( 'admin_menu', 'zcs3_add_menu_links' );

/**
 * Register Settings
 *
 * @since 1.0
 */
function zcs3_register_settings() {

	// Register Setting
	register_setting(
		'zcs3_settings_group', 			// Group name
		'zcs3_settings', 				// Setting name = html form <input> name on settings form
		'zcs3_validater_and_sanitizer'	// Input sanitizer
	);

	// Register A New Section
    add_settings_section(
        'zcs3_general_settings_section',						// ID
        __('Zencoder S3 General Settings', 'zencoder-s3'),		// Title
        'zcs3_general_settings_section_callback',				// Callback Function
        'zencoder-s3'											// Page slug
    );

	// General Settings
    add_settings_field(
        'zcs3_general_settings_field',							// ID
        __('General Settings', 'zencoder-s3'),					// Title
        'zcs3_general_settings_field_callback',					// Callback function
        'zencoder-s3',											// Page slug
        'zcs3_general_settings_section'							// Settings Section ID
    );

}
add_action( 'admin_init', 'zcs3_register_settings' );

/**
 * Validate and sanitize user input before its saved to database
 *
 * @since 1.0
 */
function zcs3_validater_and_sanitizer ( $settings ) {

	// Sanitize text field
	$settings['s3_bucket'] = sanitize_text_field($settings['s3_bucket']);

	$settings['s3_access_key'] = sanitize_text_field($settings['s3_access_key']);
	$settings['s3_access_key'] = zcs3_encrypt($settings['s3_access_key']);

	$settings['s3_secret_key'] = sanitize_text_field($settings['s3_secret_key']);
	$settings['s3_secret_key'] = zcs3_encrypt($settings['s3_secret_key']);

	$settings['zencoder_api_key'] = sanitize_text_field($settings['zencoder_api_key']);
	$settings['zencoder_api_key'] = zcs3_encrypt($settings['zencoder_api_key']);

	return $settings;
}

/**
 * Get settings from database
 *
 * @return	Array	A merged array of default and settings saved in database.
 *
 * @since 1.0
 */
function zcs3_get_settings() {

    $defaults = [
    ];

	$settings = get_option('zcs3_settings', $defaults);

    if (empty($settings['video_codecs']))
        $settings['video_codecs'] = [];

    if (empty($settings['video_sizes']))
        $settings['video_sizes'] = [];

    if (empty($settings['audio_codecs']))
        $settings['audio_codecs'] = [];

    if (!empty($settings['s3_access_key']))
        $settings['s3_access_key'] = zcs3_decrypt($settings['s3_access_key']);

    if (!empty($settings['s3_secret_key']))
        $settings['s3_secret_key'] = zcs3_decrypt($settings['s3_secret_key']);

    if (!empty($settings['zencoder_api_key']))
        $settings['zencoder_api_key'] = zcs3_decrypt($settings['zencoder_api_key']);

	return $settings;
}

/**
 * Enqueue Admin CSS and JS
 *
 * @since 1.0
 */
function zcs3_enqueue_css_js( $hook ) {
    // Load only on Starer Plugin plugin pages
	if ( $hook === "settings_page_zencoder-s3" ) {
        // Main CSS
        wp_enqueue_style( 'zcs3-admin-main-css', ZCS3_PLUGIN_URL . 'admin/css/main.css', '', ZCS3_VERSION_NUM );

        // Main JS
        // wp_enqueue_script( 'zcs3-admin-main-js', ZCS3_PLUGIN_URL . 'admin/js/main.js', array( 'jquery' ), false, true );
    } else if ( $hook === "post.php" ) {
        global $post;
        $nonce = wp_create_nonce( ZCS3_NONCE_SALT );
        wp_enqueue_script( 'zcs3-admin-post-js', ZCS3_PLUGIN_URL . 'admin/js/post.js', array( 'jquery' ), false, true );
        wp_localize_script( 'zcs3-admin-post-js', 'ZCS3', [
            'postId' => $post->ID,
            'sec' => $nonce,
            'ajax_url' => admin_url( 'admin-ajax.php' )
        ]);
    }
}
add_action( 'admin_enqueue_scripts', 'zcs3_enqueue_css_js' );

function zcs3_do_transcode_ajax_handler() {
    check_ajax_referer( ZCS3_NONCE_SALT, 'sec' );
    $res = [
        'status' => 500,
        'message' => '',
        'data' => []
    ];
    $ID = $_POST['post_id'];
    if (!isset($ID)) {
        $res['status'] = 400;
        $res['message'] = 'Post ID not set';
    } else {
        $retval = zcs3_create_transcoding_jobs($ID);
        $res['status'] = 200;
        $res['message'] = $retval;
    }

    echo json_encode($res);
    wp_die();
}
add_action( 'wp_ajax_zcs3_do_transcode', 'zcs3_do_transcode_ajax_handler');
