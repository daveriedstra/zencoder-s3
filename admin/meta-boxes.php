<?php

/**
 * Add meta boxes for attachment pages
 */
function zcs3_add_meta_boxes_attachment($post) {
    if (isset($post->post_mime_type) && strpos($post->post_mime_type, 'video') !== false) {
        add_meta_box(
            'zcs3-video-meta-box', // meta box id
            __('Zencoder S3', 'zencoder-s3'),
            'zcs3_make_video_meta_box',
            'attachment',
            'side',
            'high'
        );
    }
}
add_action('add_meta_boxes_attachment', 'zcs3_add_meta_boxes_attachment');

function zcs3_render_job_status($job_id) {
    $settings = zcs3_get_settings();
    $job_status = zcs3_get_job_status($job_id, $settings['zencoder_api_key']);

    $job = $job_status['job'];

    $created_at = $job['created_at'];
    $updated_at = $job['updated_at'];
    $input_url = $job['input_media_file']['url'];
    $state = $job['state'];

    echo "
        <p><strong>Status:</strong> $state</p>
        <p>Created at $created_at; updated at $updated_at (UTC)</p>
        <p>Input URL: <span class='zcs3-code'>$input_url</span></p>
    ";

    foreach($job['output_media_files'] as $output) {
        $output_id = $output['id'];
        $output_url = $output['url'];
        $output_state = $output['state'];
        $output_format = $output['format'];
        if (!empty($output['width']) && !empty($output['height']))
            $output_size = "{$output['width']}×{$output['height']}";
        else $output_size = '?';

        echo "
            <hr/>
            <p><strong>Output <span class='zcs3-code'>$output_id</span> :</strong></p>
            <p>Status: $output_state</p>
            <p>Size: $output_size</p>
            <p>Temporary video URL: <span class='zcs3-code'>$output_url</span></p>
        ";
    }

    return $job_status;
}

/**
 * Render the ZCS3 meta box
 */
function zcs3_make_video_meta_box($post) {
    // Helpful for reseting after test jobs
    // felt cute, might delete later
    if ( isset($_GET['zcs3-reset-transcoding-status']) )
        update_post_meta($post->ID, ZCS3_ATTACHMENT_STATUS, ZCS3_STATUS_NOT_TRANSCODED);

    $status = get_post_meta($post->ID, ZCS3_ATTACHMENT_STATUS, true);

?>
    <style type="text/css">
        .zcs3-code {
            font-family: monospace;
            padding: 0.3em 0.75em;
            display: inline-block;
            background: lightgray;
            border-radius: 0.3em;
            white-space: nowrap;
            max-width: calc(100% - 24px);
            overflow-x: auto;
            vertical-align: middle;
        }
    </style>
<?php
    if (empty($status) || $status === ZCS3_STATUS_NOT_TRANSCODED) {
?>
        <p>This media has not been queued for transcoding. Click below to transcode this media.</p>
        <p><strong>Warning:</strong> This will start multiple jobs with Zencoder as per your settings on the Zencoder S3 settings page and <em>will cost you money</em>!</p>
        <button
            class="button zcs3-button"
            id="zcs3-do-transcode-button"
        >Transcode</button>
<?php
    } else if ($status === ZCS3_STATUS_JOB_CREATED) {
        $job_id = get_post_meta($post->ID, ZCS3_ATTACHMENT_JOB_ID, true);
?>
        <p>Job has been created. This status will automatically update when the job is complete.</p>
        <p>Job ID: <span class="zcs3-code"><?php echo $job_id; ?></span></p>
<?php
        $job_status = zcs3_render_job_status($job_id);

        if ($job_status['job']['state'] == 'cancelled')
            update_post_meta($post->ID, ZCS3_ATTACHMENT_STATUS, ZCS3_STATUS_JOB_FAILED);
    } else if ($status === ZCS3_STATUS_JOB_FAILED) {
        // TODO -- give failure reason here
        $job_status = get_post_meta($post->ID, ZCS3_ATTACHMENT_STATUS, true);
        $job_id = get_post_meta($post->ID, ZCS3_ATTACHMENT_JOB_ID, true);
?>
        <p>Video transcoding failed! In the future we'll give a reason for it here. For now, click the button below to try again.</p>
        <p>Job ID: <span class="zcs3-code"><?php echo $job_id; ?></span></p>
        <p>Job status: <span class="zcs3-code"><?php echo $job_status; ?></span></p>

        <?php zcs3_render_job_status($job_id); ?>

        <p><strong>Warning:</strong> This will start multiple jobs with Zencoder as per your settings on the Zencoder S3 settings page and <em>will cost you money</em>!</p>
        <button
            class="button zcs3-button"
            id="zcs3-do-transcode-button"
        >Transcode</button>
<?php
    } else if ($status === ZCS3_STATUS_TRANSCODED) {
?>
        <p>Transcoding completed.</p>
<?php
        $linked_files = zcs3_get_linked_files($post->ID);
        foreach ($linked_files as $lf) {
            zcs3_render_output_details($lf);
        }
    } else {
        $job_status = get_post_meta($post->ID, ZCS3_ATTACHMENT_STATUS, true);
        $job_id = get_post_meta($post->ID, ZCS3_ATTACHMENT_JOB_ID, true);
?>
        <p><strong>Warning:</strong> unknown error transcoding. Contact support.</p>
        <p>Job ID: <span class="zcs3-code"><?php echo $job_id; ?></span></p>
        <p>Job status: <span class="zcs3-code"><?php echo $job_status; ?></span></p>
<?php
    }
}

function zcs3_human_filesize($bytes, $decimals = 2) {
    if ($bytes < 1024) {
        return $bytes . ' B';
    }

    $factor = floor(log($bytes, 1024));
    return sprintf("%.{$decimals}f ", $bytes / pow(1024, $factor)) . ['B', 'KB', 'MB', 'GB', 'TB', 'PB'][$factor];
}

function zcs3_human_duration($ms) {
    $sec = ($ms % 60000) / 1000;
    $min = $ms / 60000;
    return sprintf("%02.0f", $min) . ":" . sprintf("%06.3f", $sec);
}

function zcs3_render_output_details($lf) {
    $name = array_values(array_slice(explode('/', $lf['url']), -1))[0];
    $filesize = zcs3_human_filesize($lf['details']['filesize']);
    $duration = zcs3_human_duration($lf['details']['duration']);
    echo "
        <div class='zcs3-linked-file'>
            <p>
                <strong>{$name}</strong>
                <br/>
                {$lf['details']['width']} × {$lf['details']['height']}
                <br/>
                {$lf['details']['format']} / {$lf['details']['codec']}
                <br/>
                {$filesize} / {$duration}
                <a href='{$lf['url']}' target='_blank'><span class='zcs3-code'>{$lf['url']}</span></a>
            </p>
        </div>
    ";
}
