<?php
/**
 * Plugin Name: Zencoder S3
 * Plugin URI: https://gitlab.com/daveriedstra
 * Description: A plugin to automate transcoding of S3 media with Zencoder
 * Author: Dave Riedstra
 * Author URI: https://daveriedstra.com
 * Version: 0.1
 * Text Domain: zencoder-s3
 * Domain Path: /languages
 * License: GPL v2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * This plugin was developed using the WordPress starter plugin template by Arun Basil Lal <arunbasillal@gmail.com>
 * Please leave this credit and the directory structure intact for future developers who might read the code.
 * @GitHub https://github.com/arunbasillal/WordPress-Starter-Plugin
 */

/**
 * ~ Directory Structure ~
 *
 * /admin/ 						- Plugin backend stuff.
 * /functions/					- Functions and plugin operations.
 * /includes/					- External third party classes and libraries.
 * /languages/					- Translation files go here.
 * /public/						- Front end files and functions that matter on the front end go here.
 * index.php					- Dummy file.
 * license.txt					- GPL v2
 * zcs3_zencoder-s3.php	- Main plugin file containing plugin name and other version info for WordPress.
 * readme.txt					- Readme for WordPress plugin repository. https://wordpress.org/plugins/files/2018/01/readme.txt
 * uninstall.php				- Fired when the plugin is uninstalled.
 */

/**
 *
 * General idea:
 * in media uploader, button to transcode with zencoder
 *
 * in manual action
 *     - if is video && zencode settings valid, then
 *       get all zencoder formats
 *       get all zencoder sizes
 *       get zencoder settings
 *       send zencoder job queries w/ settings
 *       on response, add to ACF map for original media:
 *           sources: [{
 *             url: "...",
 *             width: 1920,
 *             height: 1080,
 *             format: "VP8",
 *             job_id: "...",
 *             status: "pending"...
 *           }, ...]
 *      create cron job
 *   - else,
 *     notify that video can't be transcoded
 *
 * in cron job:
 *     - get list of all media with pending sources
 *     - query each job status and update if needed
 *
 * options in zencoder settings page:
 *     - video sizes
 *     - video formats
 *     - audio formats
 *     - S3 bucket
 *     - zencoder API key
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Define constants
 *
 * @since 1.0
 */
if ( ! defined( 'ZCS3_VERSION_NUM' ) ) 		define( 'ZCS3_VERSION_NUM'		, '0.1' ); // Plugin version constant
if ( ! defined( 'ZCS3_PLUGIN' ) )		define( 'ZCS3_PLUGIN'		, trim( dirname( plugin_basename( __FILE__ ) ), '/' ) ); // Name of the plugin folder eg - 'zencoder-s3'
if ( ! defined( 'ZCS3_PLUGIN_DIR' ) )	define( 'ZCS3_PLUGIN_DIR'	, plugin_dir_path( __FILE__ ) ); // Plugin directory absolute path with the trailing slash. Useful for using with includes eg - /var/www/html/wp-content/plugins/zencoder-s3/
if ( ! defined( 'ZCS3_PLUGIN_URL' ) )	define( 'ZCS3_PLUGIN_URL'	, plugin_dir_url( __FILE__ ) ); // URL to the plugin folder with the trailing slash. Useful for referencing src eg - http://localhost/wp/wp-content/plugins/zencoder-s3/

// media format constants
// zencoder supports more than these but this is purpose-built and we don't need
// them. If you read this in the future on some public repo, feel free to PR and
// add them.
//
// these haven't been tested yet.

if ( ! defined( 'ZCS3_VIDEO_CODECS' ) )
    define( 'ZCS3_VIDEO_CODECS', [
        'h264' => 'H.264',
        'h265' => 'H.265',
        'vp8' => 'VP8',
        'vp9' => 'VP9'
    ]);

if ( ! defined( 'ZCS3_AUDIO_CODECS' ) )
    define( 'ZCS3_AUDIO_CODECS', [
        'mp3' => 'MP3',
        'aac' => 'AAC',
        'ogg' => 'OGG Vorbis'
    ]);

// video sizes constant

if ( ! defined( 'ZCS3_VIDEO_SIZES' ) )
    define( 'ZCS3_VIDEO_SIZES', [
        120 => '120p',
        240 => '240p',
        360 => '360p',
        480 => '480p',
        720 => '720p',
        1080 => '1080p',
        1440 => '1440p',
        2160 => '2160p'
    ]);

// Statuses
if ( ! defined( 'ZCS3_ATTACHMENT_STATUS' ) )
    define( 'ZCS3_ATTACHMENT_STATUS', 'zcs3-attachment-status');

if ( ! defined( 'ZCS3_STATUS_NOT_TRANSCODED' ) )
    define( 'ZCS3_STATUS_NOT_TRANSCODED', 'zcs3-status-not-transcoded');
if ( ! defined( 'ZCS3_STATUS_JOB_CREATED' ) )
    define( 'ZCS3_STATUS_JOB_CREATED', 'zcs3-status-job-created');
if ( ! defined( 'ZCS3_STATUS_TRANSFERRING' ) )
    define( 'ZCS3_STATUS_TRANSFERRING', 'zcs3-status-transferring');
if ( ! defined( 'ZCS3_STATUS_DOWNLOADING' ) )
    define( 'ZCS3_STATUS_DOWNLOADING', 'zcs3-status-downloading');
if ( ! defined( 'ZCS3_STATUS_UPLOADING' ) )
    define( 'ZCS3_STATUS_UPLOADING', 'zcs3-status-uploading');
if ( ! defined( 'ZCS3_STATUS_TRANSCODED' ) )
    define( 'ZCS3_STATUS_TRANSCODED', 'zcs3-status-transcoded');
if ( ! defined( 'ZCS3_STATUS_JOB_FAILED' ) )
    define( 'ZCS3_STATUS_JOB_FAILED', 'zcs3-status-job-failed');
if ( ! defined( 'ZCS3_STATUS_S3_HOSTED' ) )
    define( 'ZCS3_STATUS_S3_HOSTED', 'zcs3-status-s3-hosted');

if ( ! defined( 'ZCS3_STATUS_ERROR' ) )
    define( 'ZCS3_STATUS_ERROR', 'zcs3-status-error');

if ( ! defined( 'ZCS3_ATTACHMENT_JOB_ID' ) )
    define( 'ZCS3_ATTACHMENT_JOB_ID', 'zcs3-attachment-job-id');
if ( ! defined( 'ZCS3_LINKED_FILES' ) )
    define( 'ZCS3_LINKED_FILES', 'zcs3-linked-files');

if ( ! defined( 'ZCS3_NONCE_SALT' ) )
    define( 'ZCS3_NONCE_SALT', NONCE_SALT);
if ( ! defined( 'ZCS3_OPENSSL_KEY' ) )
    define( 'ZCS3_OPENSSL_KEY', LOGGED_IN_KEY);

if ( ! defined( 'ZCS3_ZENCODER_API_URL' ) )
    define( 'ZCS3_ZENCODER_API_URL', 'https://app.zencoder.com/api/v2/');

/**
 * Add plugin version to database
 *
 * @refer https://codex.wordpress.org/Creating_Tables_with_Plugins#Adding_an_Upgrade_Function
 * @since 1.0
 */
update_option( 'abl_zcs3_version', ZCS3_VERSION_NUM );	// Change this to add_option if a release needs to check installed version.

// Load everything
require_once( ZCS3_PLUGIN_DIR . 'loader.php' );

// Register activation hook (this has to be in the main plugin file or refer bit.ly/2qMbn2O)
register_activation_hook( __FILE__, 'zcs3_activate_plugin' );
