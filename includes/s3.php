<?php

/**
 * Gets the Authorization header for an AWS4 S3 request
 * https://developers.digitalocean.com/documentation/spaces/#authentication
 *
 * $params = [
 *  s3_config
 *  http_method
 *  path
 *  query_str
 *  payload_hash
 *  other_headers
 * ]
 */
function zcs3_get_s3_authorization($params) {
    $algo = 'sha256';
    $sig_ver = "AWS4-HMAC-SHA256";
    $yyyymmdd = date('Ymd');
    $date_iso8601 = $params['other_headers']['X-Amz-Date'];

    $access_key = $params['s3_config']['access_key'];
    $secret_key = $params['s3_config']['secret_key'];
    $region = $params['s3_config']['region'];

    $c_headers = zcs3_canonicalize_headers($params['other_headers']);
    $params['canon_headers'] = $c_headers;
    $signed_headers = implode(';', array_keys($c_headers));
    $params['signed_headers'] = $signed_headers;

    // make string to sign
    $cred_scope = "{$yyyymmdd}/{$region}/s3/aws4_request";
    $canonical = zcs3_get_s3_canonical_request($params);
    $canon = hash($algo, $canonical);
    $str_to_sign = "$sig_ver\n$date_iso8601\n$cred_scope\n$canon";

    // make signing key
    $signing_key = zcs3_get_signing_key($yyyymmdd, $secret_key, $region);

    // sign string
    $signature = hash_hmac($algo, $str_to_sign, $signing_key);

    return "$sig_ver Credential=$access_key/$cred_scope, SignedHeaders=$signed_headers, Signature=$signature";
}

function zcs3_get_signing_key($yyyymmdd, $secret_key, $region, $algo = 'sha256') {
    $date_key = hash_hmac($algo, $yyyymmdd, "AWS4{$secret_key}", true);
    $date_reg_key = hash_hmac($algo, $region, $date_key, true);
    $date_reg_svc_key = hash_hmac($algo, 's3', $date_reg_key, true);
    return hash_hmac($algo, 'aws4_request', $date_reg_svc_key, true);
}

/**
 * Generates a Canonical request which can be used to sign an S3 request
 * Takes same params as zcs3_get_authorization + signed_headers
 * Doesn't handle query params very well atm
 *
 * NTS - $path is the stuff after the endpoint before the query; generally /
 */
function zcs3_get_s3_canonical_request($params) {
    $method = strtoupper($params['method']);
    $path = zcs3_canonicalize_path($params['path']);
    $query_str = $params['query_str'];
    $headers_string = zcs3_stringify_headers($params['canon_headers']);
    $signed_headers = $params['signed_headers'];
    $payload_hash = $params['payload_hash'];

    return "{$method}\n{$path}\n{$query_str}\n{$headers_string}\n\n{$signed_headers}\n{$payload_hash}";
}

/**
 * Makes a canonical headers array according to the AWSv4 signature spec.
 */
function zcs3_canonicalize_headers($headers) {
    $zcs3_s3_header_blacklist = [
        'cache-control',
        'content-type',
        'content-length',
        'expect',
        'max-forwards',
        'pragma',
        'range',
        'te',
        'if-match',
        'if-none-match',
        'if-modified-since',
        'if-unmodified-since',
        'if-range',
        'accept',
        'authorization',
        'proxy-authorization',
        'from',
        'referer',
        'user-agent',
        'x-amzn-trace-id',
        'aws-sdk-invocation-id',
        'aws-sdk-retry',
    ];

    $c_headers = [];
    foreach ($headers as $key => $val) {
        $key = strtolower($key);
        if (!in_array($key, $zcs3_s3_header_blacklist))
            $c_headers[$key] = $val;
    }
    ksort($c_headers);
    return $c_headers;
}

function zcs3_stringify_headers($headers) {
    $s_headers = [];
    foreach($headers as $key => $val) {
        $val = preg_replace('/\s+/', ' ', $val);
        $s_headers[] = "$key:$val";
    }
    return implode("\n", $s_headers);
}

function zcs3_canonicalize_path($path) {
    $enc = rawurlencode(ltrim($path, '/'));
    return '/' . str_replace('%2F', '/', $enc);
}

/**
 * Makes a headers object for an S3 request
 */
function zcs3_make_s3_headers($method, $host, $path, $query_str, $body_len, $body_hash, $body_mimetype, $s3_config, $public = true) {
    $headers = [
        "Content-Length" => $body_len,
        "Content-Type" => $body_mimetype,
        "Host" => $host,
        "X-Amz-Content-Sha256" => $body_hash,
        "X-Amz-Date" => gmdate("Ymd\THis\Z")
    ];
    if ($public)
        $headers['X-Amz-Acl'] = 'public-read';
    $auth_params = [
        "s3_config" => $s3_config,
        "method" => $method,
        "path" => $path,
        "query_str" => $query_str,
        "payload_hash" => $body_hash,
        "other_headers" => $headers
    ];
    $auth = zcs3_get_s3_authorization($auth_params);

    $headers['Authorization'] = $auth;

    function zcs3_normalize_header($key, $val) {
        return "$key: $val";
    }

    return array_map('zcs3_normalize_header', array_keys($headers), $headers);
}

?>
