<?php

/**
 * A WP_Cron job to manually transfer Zencoder jobs that didn't make it to their
 * destination S3 server for whatever reason (ie, if Zencoder continues to only
 * support Amazon S3 but the specified server is another provider).
 *
 * @TODO: determine whether the file needs to be manually transferred
 */
function zcs3_cron_transfer() {
    // 1 get all pending jobs -- so we use only one request to zencoder
    $posts = zcs3_get_pending_jobs();

    // 2 find all with 'finished' status
    $finished = zcs3_get_finished_jobs($posts);

    // 3 cycle each
    foreach ($finished as $item) {
        // update status to transferring
        update_post_meta($item['post']->ID, ZCS3_ATTACHMENT_STATUS, ZCS3_STATUS_TRANSFERRING);

        zcs3_set_linked_files($item['post'], $item['job']);

        foreach($item['job']['output_media_files'] as $output) {
            $fname = zcs3_download_output($item['post']->ID, $output);

            // update status if download failed
            if ($fname === false) {
                zcs3_update_linked_files($item['post']->ID, $output['id'], ZCS3_STATUS_ERROR, 'Could not download file from Zencoder.');
                continue;
            }

            $result = zcs3_upload_output($item['post']->ID, $output, $fname);

            if ($result === true) {
                zcs3_update_linked_files($item['post']->ID, $output['id'], ZCS3_STATUS_S3_HOSTED);
                unlink($fname);
            } else {
                // @TODO
                // move file somewhere safer
                // try to upload later?
                zcs3_update_linked_files($item['post']->ID, $output['id'], ZCS3_STATUS_ERROR, 'Could not upload file to S3 storage.');
            }
        }

        update_post_meta($item['post']->ID, ZCS3_ATTACHMENT_STATUS, ZCS3_STATUS_TRANSCODED);
    }
}

/**
 * Gets all attachments with pending zencoder jobs
 */
function zcs3_get_pending_jobs() {
    $args = [
        'post_type' => 'attachment',
        'posts_per_page' => -1,
        'meta_query' => [[
            'key' => ZCS3_ATTACHMENT_STATUS,
            'value' => ZCS3_STATUS_JOB_CREATED,
            'compare' => '='
        ]]
    ];
    return get_posts($args);
}

/**
 * Filter a list of WP_Posts (representing pending ZCS3 attachments)
 * down to those with finished Zencoder jobs, and attach the jobs.
 * Output:
 * [
 *  'post' => WP_Post,
 *  'job' => ZencoderJob
 * ]
 */
function zcs3_get_finished_jobs($posts) {
    // get transcoding settings
    $settings = zcs3_get_settings();
    $api_key = $settings['zencoder_api_key'];

    // get all zcs3 jobs from zencoder
    $jobs = zcs3_get_job_status('', $api_key);

    // for each post, if there is a 'finished' job, add it to output
    $output = [];
    foreach($posts as $post) {
        $job_id = get_post_meta($post->ID, ZCS3_ATTACHMENT_JOB_ID, true);
        echo "<p>$job_id</p>";

        foreach($jobs as $entry) {
            $job = $entry['job'];
            if (intval($job['id']) === intval($job_id)) {
                if ($job['state'] === 'finished') {
                    $output[] = [
                        'post' => $post,
                        'job' => $job
                    ];
                }
                break;
            }
        }
    }

    return $output;
}

/**
 * Attempts to download the output file to a temporary location on this
 * filesystem. Updates ZCS3_LNKED_FILES for any errors.
 */
function zcs3_download_output($post_id, $output) {
    if ($output['state'] !== 'finished') {
        zcs3_update_linked_files($post_id, $output['id'], ZCS3_STATUS_ERROR, $output['state']);
        return false;
    }

    if (!zcs3_can_hold_file($output['file_size_bytes'])) {
        zcs3_update_linked_files($post_id, $output['id'], ZCS3_STATUS_INSUFFICIENT_SPACE, "Not enough space on the WordPress host to temporarily hold this file for transfer.");
        return false;
    }

    zcs3_update_linked_files($post_id, $output['id'], ZCS3_STATUS_DOWNLOADING);
    $fname = wp_tempnam();
    $status = zcs3_download($output['url'], $fname);

    if ($status !== true) {
        zcs3_update_linked_files($post_id, $output['id'], ZCS3_STATUS_ERROR, $status);
        return false;
    }

    return $fname;
}

/**
 * Uploads a file to the S3 server
 */
function zcs3_upload_output($post_id, $output, $tmp_fname) {
    // update linked files to uploading
    zcs3_update_linked_files($post_id, $output['id'], ZCS3_STATUS_UPLOADING);

    // make auth headers
    $uploaded_fname = $output['label'];
    $payload_hash = hash_file('sha256', $tmp_fname);

    if (!($s3_config = zcs3_get_s3_config()))
        return "Could not get or understand settings. Please ensure settings are properly configured.";

    $endpoint = "{$s3_config['bucket']}.{$s3_config['region']}.{$s3_config['host']}";
    $endpoint = untrailingslashit($endpoint); // thanks wordpress

    $folder = $s3_config['folder'];
    if ($folder !== "")
        $folder = trailingslashit($folder);

    $uploaded_fname = $folder . $uploaded_fname;

    // DigitalOcean has a hard time with @
    if (strpos(strtolower($s3_config['host']), 'digitalocean') !== false) {
        $uploaded_fname = preg_replace('/\@/', 'at', $uploaded_fname);
    }

    $file_url = "{$endpoint}/{$uploaded_fname}";

    $s3_headers = zcs3_make_s3_headers(
        'PUT',
        $endpoint,
        "/$uploaded_fname",
        "",
        filesize($tmp_fname),
        $payload_hash,
        mime_content_type($tmp_fname),
        $s3_config
    );

    // do the upload
    $status = zcs3_put_file($file_url, $tmp_fname, $s3_headers);

    // return true on success; zcs3_put_file result on failure
    if (is_string($status) || $status['http_code'] !== 200) {
        return $status;
    } else {
        // update linked file appropriately
        if (strpos($file_url, '://') === false)
            $file_url = "https://$file_url";
        zcs3_set_linked_file_url($post_id, $output['id'], $file_url);
        return true;
    }
}

/**
 * Returns the S3 configuration
 * or false if it can't
 */
function zcs3_get_s3_config() {
	$settings = zcs3_get_settings();

    // parse url into chunks
    try {
        $url = $settings['s3_bucket'];
        if (strpos($url, '://') !== false)
            $url = explode('://', $url)[1];

        $folder = '';
        if (strpos($url, '/') !== false) {
            $chunks = explode('/', $url);
            $url = $chunks[0];
            $folder = $chunks[1];
        }

        $chunks = explode('.', $url);
        $bucket = $chunks[0];
        $region = $chunks[1];
        $host = "{$chunks[2]}.{$chunks[3]}";

        $config = [
            'bucket' => $bucket,
            'region' => $region,
            'host' => $host,
            'folder' => $folder,
            'access_key' => $settings['s3_access_key'],
            'secret_key' => $settings['s3_secret_key']
        ];
    } catch (Exception $e) {
        return false;
    }
    return $config;
}

?>
