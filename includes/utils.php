<?php

/**
 * Simple HTTP(S) POST request. Args are self-explanatory.
 */
function zcs3_post($endpoint, $body, $header = []) {
    $ch = curl_init($endpoint);
    $data = json_encode($body);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);

    if ($response === false) {
        $response = curl_error($ch);
    } else {
        $response = json_decode($response, true);
    }

    curl_close($ch);

    return $response;
}

/**
 * Same but for GET. $query can be a string or key => value pairs.
 * No url encoding baked in so far.
 */
function zcs3_get($endpoint, $query = "", $header = []) {
    if (!is_string($query)) {
        $pairs = [];
        foreach ($query as $key => $value) {
            $pairs[] = "$key=$value";
        }
        $query = implode($pairs, "&");
    }

    if (!empty($query)) {
        $endpoint = "$endpoint?$query";
    }

    $ch = curl_init($endpoint);

    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);

    if ($response === false) {
        $response = curl_error($ch);
    }

    curl_close($ch);

    return $response;
}

/**
 * Simple HTTP(S) PUT request to send a file. Args are self-explanatory.
 *
 * Returns [http_code, response] or curl_error string
 */
function zcs3_put_file($endpoint, $fpath, $header = []) {
    $ch = curl_init($endpoint);

    curl_setopt($ch, CURLOPT_PUT, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 50);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

    $file = fopen($fpath, "rb");
    curl_setopt($ch, CURLOPT_INFILE, $file);
    curl_setopt($ch, CURLOPT_INFILESIZE, filesize($fpath));

    $response = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $ret = ['http_code' => $http_code, 'response' => $response];

    if ($response === false)
        $ret = curl_error($ch);

    curl_close($ch);
    fclose($file);

    return $ret;
}


/**
 * Downloads a file with cURL directly to disk.
 */
function zcs3_download($url, $full_path, $headers = []) {
    set_time_limit(0);

    //This is the file where we save the information
    $fp = fopen($full_path, 'w+');

    //Here is the file we are downloading, replace spaces with %20
    $ch = curl_init(str_replace(" ", "%20", $url));
    curl_setopt($ch, CURLOPT_TIMEOUT, 50);

    // write curl response to file
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    // get curl response
    $res = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // check for execution errors
    if ($res === false) {
        $res = curl_error($ch);
    } else if ($http_code !== 200) {
        $res = "HTTP $http_code";
    }

    curl_close($ch);
    fclose($fp);

    return $res;
}

/**
 * Checks if the filesystem is greater than $fsize_bytes and an optional buffer
 * (default buffer = 20mb);
 */
function zcs3_can_hold_file($fsize_bytes, $buffer = 20 * 1000 * 1000) {
    $tmp_dir = get_temp_dir(); // thanks, wp
    $free = disk_free_space($tmp_dir);
    return $free > ($fsize_bytes + $buffer);
}

/**
 * makes a temporary file; returns the full name
 * make sure filesystem is inited before with
 * WP_Filesystem()
 */
function zcs3_put_tmp_file($contents, $name = '') {
    global $wp_filesystem;
    $fname = wp_tempnam($name);
    $res = $wp_filesystem->put_contents( $fname, $contents, FS_CHMOD_FILE);
    if (!$res)
        return false;
    return $fname;
}

/**
 * deletes a file
 * returns true on success
 * make sure filesystem is inited before with
 * WP_Filesystem()
 */
function zcs3_delete_file($fname) {
    global $wp_filesystem;
    return $wp_filesystem->delete($fname);
}

?>
