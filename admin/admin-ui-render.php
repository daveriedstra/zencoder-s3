<?php
/**
 * Admin UI setup and render
 *
 * @since 1.0
 * @function	zcs3_general_settings_section_callback()	Callback function for General Settings section
 * @function	zcs3_general_settings_field_callback()	Callback function for General Settings field
 * @function	zcs3_admin_interface_render()				Admin interface renderer
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Callback function for General Settings section
 *
 * @since 1.0
 */
function zcs3_general_settings_section_callback() {
	echo '<p>' . __('These settings will be applied to each transcoding job.', 'zencoder-s3') . '</p>';
}

/**
 * Callback function for General Settings field
 *
 * @since 1.0
 */
function zcs3_general_settings_field_callback() {

	// Get Settings
	$settings = zcs3_get_settings();

	// General Settings. Name of form element should be same as the setting name in register_setting(). ?>

	<fieldset>

		<!-- S3 bucket -->
        <div class="zcs3-input-group">
            <input
                type="text"
                name="zcs3_settings[s3_bucket]"
                class="regular-text"
                value="<?php if ( isset( $settings['s3_bucket'] ) && ( ! empty($settings['s3_bucket']) ) ) echo esc_attr($settings['s3_bucket']); ?>"
            />
            <p class="zcs3-description description"><?php _e('The full URL of the S3 bucket where the transcoded file should end up. It should look like this:', 'zencoder-s3'); ?> <code>mybucket.nyc3.mys3host.com</code></p>
        </div>

		<!-- S3 access key -->
        <div class="zcs3-input-group">
            <input
                type="password"
                name="zcs3_settings[s3_access_key]"
                class="regular-text"
                value="<?php if ( isset( $settings['s3_access_key'] ) && ( ! empty($settings['s3_access_key']) ) ) echo esc_attr($settings['s3_access_key']); ?>"
            />
            <p class="zcs3-description description"><?php _e('Your S3 access key.', 'zencoder-s3'); ?></p>
        </div>

		<!-- S3 secret key -->
        <div class="zcs3-input-group">
            <input
                type="password"
                name="zcs3_settings[s3_secret_key]"
                class="regular-text"
                value="<?php if ( isset( $settings['s3_secret_key'] ) && ( ! empty($settings['s3_secret_key']) ) ) echo esc_attr($settings['s3_secret_key']); ?>"
            />
            <p class="zcs3-description description"><?php _e('Your S3 secret key.', 'zencoder-s3'); ?></p>
        </div>

		<!-- Zencoder API key -->
        <div class="zcs3-input-group">
            <input
                type="password"
                name="zcs3_settings[zencoder_api_key]"
                class="regular-text"
                value="<?php if ( isset( $settings['zencoder_api_key'] ) && ( ! empty($settings['zencoder_api_key']) ) ) echo esc_attr($settings['zencoder_api_key']); ?>"
            />
            <p class="zcs3-description description"><?php _e('Your Zencoder API key.', 'zencoder-s3'); ?></p>
        </div>

        <div class="zcs3-input-group">
            <!-- video codecs -->
            <?php foreach (ZCS3_VIDEO_CODECS as $codec_val => $codec_name): ?>
                <label class="zcs3-checkbox-label">
                    <input
                        type="checkbox"
                        name="zcs3_settings[video_codecs][<?php echo $codec_val; ?>]"
                        class="regular-checkbox"
                        value="1"
                <?php
                    if ( isset( $settings['video_codecs'][$codec_val] ) )
                        checked( '1', $settings['video_codecs'][$codec_val] );
                ?>
                    />
                    <?php echo $codec_name; ?>
                </label>
                <br />
            <?php endforeach; ?>
            <p class="zcs3-description description"><?php _e('The codecs each video will be transcoded to.', 'zencoder-s3'); ?></p>
        </div>

        <div class="zcs3-input-group">
            <!-- video sizes -->
            <?php foreach (ZCS3_VIDEO_SIZES as $size_val => $size_name): ?>
                <label class="zcs3-checkbox-label">
                    <input
                        type="checkbox"
                        name="zcs3_settings[video_sizes][<?php echo $size_val; ?>]"
                        class="regular-checkbox"
                        value="1"
                <?php
                    if ( isset( $settings['video_sizes'][$size_val] ) )
                        checked( '1', $settings['video_sizes'][$size_val] );
                ?>
                    />
                    <?php echo $size_name; ?>
                </label>
                <br />
            <?php endforeach; ?>
            <p class="zcs3-description description"><?php _e('The sizes generated for each transcoding job.', 'zencoder-s3');?></p>
        </div>

        <div class="zcs3-input-group">
            <!-- audio codecs -->
            <?php foreach (ZCS3_AUDIO_CODECS as $codec_val => $codec_name): ?>
                <label class="zcs3-checkbox-label">
                    <input
                        type="checkbox"
                        name="zcs3_settings[audio_codecs][<?php echo $codec_val; ?>]"
                        class="regular-checkbox"
                        value="1"
                <?php
                    if ( isset( $settings['audio_codecs'][$codec_val] ) )
                        checked( '1', $settings['audio_codecs'][$codec_val] );
                ?>
                    />
                    <?php echo $codec_name; ?>
                </label>
                <br />
            <?php endforeach; ?>
            <p class="zcs3-description description"><?php _e('The codecs each audio file will be transcoded to.', 'zencoder-s3'); ?></p>
        </div>

	</fieldset>
	<?php
}

/**
 * Admin interface renderer
 *
 * @since 1.0
 */
function zcs3_admin_interface_render () {

	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}

    if ( isset($_GET['zcs3-cron']) ) {
        echo "running cron 4 u...";
        zcs3_cron_transfer();
    }

	/**
	 * If settings are inside WP-Admin > Settings, then WordPress will automatically display Settings Saved. If not used this block
	 * @refer	https://core.trac.wordpress.org/ticket/31000
	 * If the user have submitted the settings, WordPress will add the "settings-updated" $_GET parameter to the url
	 *
	if ( isset( $_GET['settings-updated'] ) ) {
		// Add settings saved message with the class of "updated"
		add_settings_error( 'zcs3_settings_saved_message', 'zcs3_settings_saved_message', __( 'Settings are Saved', 'zencoder-s3' ), 'updated' );
	}

	// Show Settings Saved Message
	settings_errors( 'zcs3_settings_saved_message' ); */?>

	<div class="wrap">
		<h1>Zencoder S3</h1>

		<form action="options.php" method="post">
			<?php
			// Output nonce, action, and option_page fields for a settings page.
			settings_fields( 'zcs3_settings_group' );

			// Prints out all settings sections added to a particular settings page.
			do_settings_sections( 'zencoder-s3' );	// Page slug

			// Output save settings button
			submit_button( __('Save Settings', 'zencoder-s3') );
			?>
		</form>
	</div>
	<?php
}
